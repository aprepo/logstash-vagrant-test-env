cd modules

# This only works on bash 4 and abobe
declare -A PACKAGES
PACKAGES['file_concat']="ispavailability-file_concat-0.1.0"
PACKAGES['stdlib']="puppetlabs-stdlib-4.3.2"
PACKAGES['logstash']="elasticsearch-logstash-0.5.1"

echo "Downloading modules..."
for package in "${!PACKAGES[@]}"
do
	PACKAGE_DIR=${PACKAGES[${package}]}
	TAR_FILE=${PACKAGE_DIR}.tar.gz
	URL="https://forgeapi.puppetlabs.com/v3/files/${TAR_FILE}"
	
	echo "Downloading: ${URL}"
	wget ${URL}
	
	echo "Unpacking: ${TAR_FILE}"
	tar xzf ${TAR_FILE}
	
	echo "Removing tar file: ${TAR_FILE}"
	rm ${TAR_FILE}
	
	if [ -h $package ]
	then
		echo "Removing old symlink ${package}"
		rm ${package}
	fi
	echo "Creating symlink: ${package}"
	ln -s ${PACKAGE_DIR} ${package}
done

echo "Done."